using Godot;

public class Ball : RigidBody2D
{
    private const int ForceFactor = 1000;

    private void OnGuiInput(object @event)
    {
        var direction = new Vector2((float)GD.RandRange(-ForceFactor, ForceFactor),
            (float)GD.RandRange(-ForceFactor, ForceFactor));
        var panel = GetNode<Panel>("Panel");
        ApplyImpulse(panel.RectSize / 2, direction);
    }
}
