using Godot;
using System;

public class Main : Panel
{
    private void OnGuiInput(object @event)
    {
        if (!(@event is InputEventMouseButton button)) return;
        if (!button.Pressed) return;
        
        foreach (var child in GetChildren())
        {
            if (!(child is Ball ball)) continue;
            var duplicate = ball.Duplicate();
            AddChild(duplicate);
        }
    }
}


