using Godot;

[Tool]
public class Wall : ColorRect
{
    public override void _Ready()
    {
        var collisionShape = GetNode<CollisionShape2D>("StaticBody2D/CollisionShape2D");
        collisionShape.Position = RectSize / 2; 
        var rectangle = (RectangleShape2D)collisionShape.Shape;
        rectangle.Extents = RectSize / 2;
    }
}
